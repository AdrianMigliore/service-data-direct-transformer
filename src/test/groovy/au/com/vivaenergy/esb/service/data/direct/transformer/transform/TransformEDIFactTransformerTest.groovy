package au.com.vivaenergy.esb.service.data.direct.transformer.transform

import org.junit.Test

class TransformEDIFactTransformerTest {

	@Test
	void 'test transform EDIfact to xml'() {
		def s = new TranformEDIFactTransformer()

		String txt = getClass().getResourceAsStream('/in/test.x12').text

		String xml = s.transformFromEdi(txt)

		assert xml.startsWith('''<?xml version="1.0" encoding="UTF-8"?>\r\n<X12>''')
	}

	@Test
	void 'test transform xml to EDIfact'() {
		def s = new TranformEDIFactTransformer()

		String txt = getClass().getResourceAsStream('/in/test.xml').text

		String edi = s.transformFromXml(txt)

		assert edi.startsWith('''ISA''')
	}



}
