package au.com.vivaenergy.esb.service.data.direct.transformer.route

import au.com.vivaenergy.esb.test.blueprint.AbstractBlueprintTest
import org.apache.camel.component.jasypt.JasyptPropertiesParser
import org.apache.camel.util.KeyValueHolder

class BlueprintTest extends AbstractBlueprintTest {

	@Override
	protected String setConfigAdminInitialConfiguration(Properties props) {
		// TODO props.put 'some property placeholder', 'some value'
		
		return 'au.com.vivaenergy.esb.service.data.direct.transformer'
	}

	@Override
	protected void addServicesOnStartup(Map<String, KeyValueHolder<Object, Dictionary>> services) {
		services.put 'org.apache.camel.component.properties.AugmentedPropertyNameAwarePropertiesParser',
			asService(new JasyptPropertiesParser(password: 'whatever'), 'vivatype', 'jasypt-property-parser')
	}

}