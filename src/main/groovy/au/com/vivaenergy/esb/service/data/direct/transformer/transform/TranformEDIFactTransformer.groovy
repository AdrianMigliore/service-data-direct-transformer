package au.com.vivaenergy.esb.service.data.direct.transformer.transform

import com.ddtek.xmlconverter.*

import javax.xml.transform.stream.StreamResult
import javax.xml.transform.stream.StreamSource

class TranformEDIFactTransformer {

	static String transformFromEdi(String txt) {
		Converter toXML = new ConverterFactory().newConvertToXML("converter:EDI")

		ByteArrayOutputStream out = new ByteArrayOutputStream()

		toXML.convert(
			new StreamSource(new ByteArrayInputStream(txt.bytes)),
			new StreamResult(out)
		)

		return new String(out.toByteArray())
	}

	static String transformFromXml(String txt) {
		Converter toEDI = new ConverterFactory().newConvertFromXML("converter:EDI")

		ByteArrayOutputStream out = new ByteArrayOutputStream()

		toEDI.convert(
			new StreamSource(new ByteArrayInputStream(txt.bytes)),
			new StreamResult(out)
		)

		return new String(out.toByteArray())
	}


}
